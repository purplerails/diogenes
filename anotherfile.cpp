// Diogenes: World's Simplest Unit Testing "Framework"?
// Purplerails <ranga@purplerails.com>
//
// Released under the GNU GPL version 2.

#include "diogenes.h"
#include <iostream>

using std::cout;
using std::endl;

static void ClearAllValues(int& i) { i = 2; }

static DioTest ClearInt = []() {
  int i = 12;
  cout << "before: " << i << endl;
  ClearAllValues(i);
  cout << "after: " << i << endl;
  DioExpect(i == 2);
};
